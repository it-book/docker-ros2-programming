# -*- coding: utf-8 -*-
# 「Dockerで学ぶROS2プログラミング入門」（科学情報出版）
# https://gitlab.com/it-book/docker-ros2-programming
#
# @product   Turtlesimシミュレータ用 launchファイル
# @author    Hiroyuki Okada <hiroyuki.okada@okadanet.org>
# @copyright 2020 Hiroyuki Okada
# @license   https://www.apache.org/licenses/LICENSE-2.0 Apache-2.0
#
import launch.actions
import launch_ros.actions
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([

        # Launch時にメッセージを出力する
        launch.actions.LogInfo(
            msg="Launch turtlesim node and turtle_teleop_key node."),

        # Launchしてから3秒後にメッセージを出力する
        launch.actions.TimerAction(period=3.0,actions=[
            launch.actions.LogInfo(
                msg="It's been three minutes since the launch."),
            ]),

        # Turtlesim シミュレータの起動
        Node(
            package='turtlesim',
            node_namespace='turtlesim',
            node_executable='turtlesim_node',
            node_name='turtlesim',
            output='screen',
            #黄色
            parameters=[{'background_r':255},{'background_g':255},{'background_b':0},]
        ),

        # Turtlesim をキーボードで操作するノードを起動
        Node(
            package='turtlesim',
            node_namespace='turtlesim',
            node_executable='turtle_teleop_key',
            node_name='teleop_turtle',          
            prefix="xterm -e"
        ),
    ])
