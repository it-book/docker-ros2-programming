# -*- coding: utf-8 -*-
# 「Dockerで学ぶROS2プログラミング入門」（科学情報出版）
# https://gitlab.com/it-book/docker-ros2-programming
#
# @product   シミュレータの亀の向きを変えるプログラム
# @author    Hiroyuki Okada <hiroyuki.okada@okadanet.org>
# @copyright 2020 Hiroyuki Okada
# @license   https://www.apache.org/licenses/LICENSE-2.0 Apache-2.0
#
import rclpy
from rclpy.node import Node
from rclpy.action import ActionClient
from turtlesim.action import RotateAbsolute

class RotateTurtle(Node):
    def __init__(self):
        super().__init__('rotate_turtle')
        self._action_client = ActionClient(
		self, RotateAbsolute, '/turtle1/rotate_absolute')

    def send_goal(self, theta):
        goal_msg = RotateAbsolute.Goal()
        goal_msg.theta = theta
     
        self._action_client.wait_for_server()
        self._action_client.send_goal_async(goal_msg)

def main(args=None):
    rclpy.init(args=args)

    action_client = RotateTurtle()
    theta=1.0
    action_client.send_goal(theta)

if __name__ == '__main__':
    main()
