# -*- coding: utf-8 -*-
# 「Dockerで学ぶROS2プログラミング入門」（科学情報出版）
# https://gitlab.com/it-book/docker-ros2-programming
#
# @product   シミュレータの亀を追加するプログラム
# @author    Hiroyuki Okada <hiroyuki.okada@okadanet.org>
# @copyright 2020 Hiroyuki Okada
# @license   https://www.apache.org/licenses/LICENSE-2.0 Apache-2.0
#
import rclpy
from turtlesim.srv import Spawn

def main(args=None):
    rclpy.init(args=args)
    node = rclpy.create_node('spawn_client')
    client = node.create_client(Spawn, '/spawn')

    req = Spawn.Request()
    req.x = 2.0             #x座標
    req.y = 2.0             #y座標
    req.theta = 0.2         #姿勢
    req.name = 'new_turtle' #新しい亀の名前

    # サービスが開始されるまで待つ
    while not client.wait_for_service(timeout_sec=1.0):
        node.get_logger().info('service not available, waiting again...')

    # サービスを非同期に呼び出す
    future = client.call_async(req)
    rclpy.spin_until_future_complete(node, future)
    try:
        result = future.result()
    except Exception as e:
        node.get_logger().info('Service call failed %r' % (e,))
    else:
        node.get_logger().info(
            'Result of x, y,theta, name: %f %f %f %s' %
            (req.x, req.x, req.theta, result.name))

    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
