from setuptools import setup

package_name = 'my_turtle_pkg'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml','launch/myTurtlesim.launch.py']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='root',
    maintainer_email='root@todo.todo',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
		'move=my_turtle_pkg.moveTurtle:main',
		'spawn=my_turtle_pkg.spawnTurtle:main',
		'bg_color=my_turtle_pkg.bg_paramTurtle:main',
		'rotate=my_turtle_pkg.rotateTurtle:main',
        ],
    },
)
