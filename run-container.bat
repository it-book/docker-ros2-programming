@echo off
rem  「Dockerで学ぶROS2プログラミング入門」（科学情報出版）
rem https://gitlab.com/it-book/docker-ros2-programming
rem
rem @product   Dockerイメージからコンテナを実行するバッチファイル
rem @author    Hiroyuki Okada <hiroyuki.okada@okadanet.org>
rem @copyright 2020 Hiroyuki Okada
rem @license   https://www.apache.org/licenses/LICENSE-2.0 Apache-2.0
rem
docker run -it -e DISPLAY="host.docker.internal:0.0" --rm  -v %CD%/myProjects:/root/myProjects -w /root  okdhryk/ros2docker
