# 「Dockerで学ぶROS2プログラミング入門」（科学情報出版）
# https://gitlab.com/it-book/docker-ros2-programming
# インストール環境
#    ubuntu:18.04
#    Python3
#
# General guidelines and recommendations for writing 'Dockerfile':
# https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/
#
# @product   4章で使う簡単なDockerfileの例
# @author    Hiroyuki Okada <hiroyuki.okada@okadanet.org>
# @copyright 2020 Hiroyuki Okada
# @license   https://www.apache.org/licenses/LICENSE-2.0 Apache-2.0

# 基本となるイメージ
FROM ubuntu:18.04

# ユーザの情報
LABEL maintainer="Hiroyuki Okada <hiroyuki.okada@okadanet.org>"
ENV TZ JST-9
SHELL ["/bin/bash", "-c"]

# 必要なパッケージのインストール
RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip

# コンテナ実行時にpython3を実行する
ENTRYPOINT ["python3"]

